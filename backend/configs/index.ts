import { ConfigTemplate } from "./default";

export * from "./default";

/** A map of configuration objects by provider. */
export const configByProvider: { [provider: string]: ConfigTemplate } = {
    "example": require("./example").default,
    "example2": require("./example2").default
};
