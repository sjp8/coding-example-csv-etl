import { isValid, parseISO } from "date-fns";

/**
 * A transformer is a basic type which takes an arbitrary `input` of unknown type
 * and returns either an output of the given type, or null if the input was invalid or not given.
 */
export type Transformer<T> = (input: any) => T | null;

export type ColumnDefinition<T> = { transform: Transformer<T> };

/** A basic transformer which converts the input to a string. */
export const stringTransformer: Transformer<string> = (input: any) => input ? `${input}` : null;

/** A basic transformer which converts the input to a number. */
export const numberTransformer: Transformer<number> = (input: any) => isNaN(Number(input)) ? null : Number(input);

/** A basic transformer which converts the input to a Date. */
export const dateTransformer: Transformer<Date> = (input: any) => {
    const isoParsed = parseISO(input);
    const dateParsed = new Date(input);
    return [isoParsed, dateParsed].find(isValid) || new Date();
};

/**
 * A basic transformer factory which returns a number transformer based on
 * whether the input is expected to be in cents or dollars.
 */
export const currencyTransformer = (inputType: "dollars" | "cents"): Transformer<number> => (input: any): number | null => {
    const parsed = numberTransformer(input);
    return parsed !== null ? (inputType === "cents" ? parsed / 100 : parsed) : null;
}

/**
 * A configuration template for transforming one row of data from a
 * particular data source.
 */
export interface ConfigTemplate {
    /** Whether the CSV file has a header line that should be excluded from bulk inserts. */
    hasHeader: boolean;

    /** The CSV delimiter, e.g. "," */
    delimiter: string;

    /** The CSV newline character or an array of characters. */
    newline: string | string[];

    /**
     * The order of columns in the CSV file (using the column name keys below, e.g.
     * `["uuid", "make", "", "vin", "model", ...]`). An empty string element indicates a column
     * in the file that should be ignored.
     */
    columnOrder: string[];

    /** How to read and interpret each column. */
    columns: {
        /** The row's unique ID. */
        uuid: ColumnDefinition<string>;
        /** Alphanumerical Vehicle ID. */
        vin: ColumnDefinition<string>;
        /** The vehicle make. */
        make: ColumnDefinition<string>;
        /** The vehicle model. */
        model: ColumnDefinition<string>;
        /** The vehicle mileage. */
        mileage: ColumnDefinition<number>;
        /** The vehicle year. */
        year: ColumnDefinition<number>;
        /** The price in dollars. */
        price: ColumnDefinition<number>;
        /** The vehicle's registered zip code. */
        zipcode: ColumnDefinition<string>;
        /** The record's creation date. */
        created: ColumnDefinition<Date>;
        /** The record's last updated date. */
        updated: ColumnDefinition<Date>;
    };
}

/**
 * The default transformer which can be optionally extended when creating a
 * new ConfigTemplate implementation.
 */
export const defaultConfig: ConfigTemplate = {
    hasHeader: true,
    delimiter: ",",
    newline: ["\n", "\r", "\r\n"],
    columnOrder: ["uuid", "vin", "make", "model", "mileage", "year", "price", "zipcode", "created", "updated"],

    columns: {
        uuid: { transform: stringTransformer },
        vin: { transform: stringTransformer },
        make: { transform: stringTransformer },
        model: { transform: stringTransformer },
        mileage: { transform: numberTransformer },
        year: { transform: numberTransformer },
        price: { transform: numberTransformer },
        zipcode: { transform: stringTransformer },
        created: { transform: dateTransformer },
        updated: { transform: dateTransformer }
    }
};
