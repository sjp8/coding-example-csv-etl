
import { defaultConfig, ConfigTemplate, currencyTransformer } from "./index";
import { stringTransformer } from "./default";

/** Example configuration for "example" provider. */
const config: ConfigTemplate = {
    ...defaultConfig,
    hasHeader: true,
    columnOrder: ["uuid", "make", "model", "", "year"],
    columns: {
        ...defaultConfig.columns,
        /** The example provider uses a cents-based currency format. */
        price: { transform: currencyTransformer("cents") },
        
        /** The id column is a number, transform it into a string to be compatible with uuid. */
        uuid: { transform: stringTransformer }
    }
}

export default config;
