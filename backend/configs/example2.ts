
import { defaultConfig, ConfigTemplate } from "./index";
import { stringTransformer } from "./default";

/** Example configuration for "example" provider. */
const config: ConfigTemplate = {
    ...defaultConfig,
    hasHeader: false,
    delimiter: ":",
    columnOrder: ["uuid", "make", "model", "", "year"],
    columns: {
        ...defaultConfig.columns,

        /** The id column is a number, transform it into a string. */
        uuid: { transform: stringTransformer }
    }
}

export default config;
