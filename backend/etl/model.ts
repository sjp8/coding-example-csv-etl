import { Schema } from 'mongoose';
import mongoose from './database';

export const vehicleSchema = new Schema({
    uuid: String,
    vin: String,
    make: String,
    model: String,
    mileage: Number,
    year: Number,
    price: Number,
    zipcode: String,
    created: Date,
    updated: Date
}, {
    _id: false,
    
});

export const Vehicle = mongoose.model('Vehicle', vehicleSchema);
