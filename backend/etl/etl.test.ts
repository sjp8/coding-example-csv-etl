import { join } from "path";
import { loadFileIntoDatabase } from "./index";
import { Vehicle } from "./model";
import database, { server } from "./database";
import { configByProvider } from "../configs";

export default describe("CSV ETL", () => {
    test("loads a csv file with header into database", async () => {
        await loadFileIntoDatabase(join(__dirname, "../testfiles/test.csv"), configByProvider.example);
        const vehicles: any[] = await Vehicle.find().lean();
        console.log("vehicles", JSON.stringify(vehicles));

        expect(vehicles).toHaveLength(3);
        expect(vehicles[0].make).toBe("Honda");
        expect(vehicles[0].year).toBe(2003);
        expect(vehicles[2].model).toBe("Mustang");
    });

    test("loads a larger csv file without header into database", async () => {
        await loadFileIntoDatabase(join(__dirname, "../testfiles/test-noheader.csv"), configByProvider.example2);
        const vehicles: any[] = await Vehicle.find().lean();
        expect(vehicles).toHaveLength(1059);
        expect(vehicles[0].make).toBe("Honda");
        expect(vehicles[1058].model).toBe("Mustang");
    });

    afterEach(async () => {
        await Vehicle.deleteMany({});
    });

    afterAll(async () => {
        await database.disconnect();
        await server.stop();
    });
});
