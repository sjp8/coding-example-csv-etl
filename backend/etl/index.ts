
import database from "./database";
// import { Vehicle } from "./model";
import { ConfigTemplate } from "../configs";
import { createReadStream } from "fs";
import parse from "csv-parse/lib/sync";
import { Options as CSVOptions } from "csv-parse";
const etl: any = require("../lib/etl");

/** The batch size for mongoDB inserts. */
export const batchSize = Number(process.env.MONGODB_BATCH_SIZE) || 100;

/**
 * Loads a CSV file into the database using the given config.
 * 
 * @param filename The full path/name of the CSV file.
 * @param config The config with which to parse the CSV file.
 */
export async function loadFileIntoDatabase(filename: string, config: ConfigTemplate) {
    let rowIndex = 0;

    const csvOptions: CSVOptions = {
        trim: true,
        delimiter: config.delimiter,
        record_delimiter: config.newline
    };

    return await createReadStream(filename)
        // split by line
        .pipe(etl.split())

        // convert the csv line into an object using `config`
        .pipe(etl.map(function (this: any, { text }: { text: string }) {
            if (!(rowIndex === 0 && config.hasHeader)) {
                // parse the CSV line
                const ary = parse(text, csvOptions)[0];

                if (ary && ary.length) { // skip empty lines
                    const obj: { [key: string]: any } = {};
                    // copy the elements of the csv line into an object with the correct column names.
                    config.columnOrder.forEach((column, index) => {
                        if (column) { // null columns in config mean "skip this column"
                            const value = ary[index];
                            // transform the value using the configured transformer for the column.
                            const transformedValue = (config.columns as any)[column].transform(value);
                            obj[column] = transformedValue;
                        }
                    });

                    // pass the constructed object downstream
                    this.push(obj);
                }
            }
            rowIndex++;
        }))

        // insert in batches of `batchSize` records
        .pipe(etl.collect(batchSize))

        // insert into mongodb
        .pipe(etl.mongo.insert(database.connection.collection("vehicles")))
        // .pipe(etl.inspect())

        // promisify
        .promise();
}
