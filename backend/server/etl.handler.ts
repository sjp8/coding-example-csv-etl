import express from "express";
import { configByProvider } from "../configs";
import fileUpload from "express-fileupload";
import { loadFileIntoDatabase } from "../etl";
import { Vehicle } from "../etl/model";

export default async (req: express.Request, res: express.Response) => {
    const file = req.files!.file as fileUpload.UploadedFile;
    const { provider } = req.query!;
    if (!provider || typeof provider !== "string") {
        return res.status(400).json({
            error: "Must include a `provider` argument in querystring.",
            parameter: "provider"
        });
    }

    const config = configByProvider[provider];
    if (!config) {
        return res.status(400).json({
            error: "The given `provider` does not match any configuration templates.",
            parameter: "provider"
        });
    }

    await loadFileIntoDatabase(file.tempFilePath, config);
    const vehicles = await Vehicle.find().lean().exec();

    return res.json({ vehicles });
}