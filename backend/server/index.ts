import express from "express";
import fileUpload from "express-fileupload";
import cors from "cors";
import etlHandler from "./etl.handler";

const app = express();
app.use(cors());
app.use(fileUpload({
    useTempFiles: true
}));

app.post("/api/etl/csv", etlHandler);

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`API started on ${port}`));
