import React from "react";

/** A table of vehicles. */
export const VehiclesTable = ({ vehicles }: { vehicles: any[] }) => (
    <article>
        <h3>Uploaded Vehicles</h3>
        <div>{vehicles.length} Vehicles</div>
        <table>
            <thead>
                <tr>
                    {Object.keys(vehicles[0] || {}).map(col => <th key={col}>{col}</th>)}
                </tr>
            </thead>
            <tbody>
                {vehicles.map(vehicle => <tr key={vehicle._id}>
                    {Object.keys(vehicle).map(col => <td key={col}>{vehicle[col]}</td>)}
                </tr>)}
            </tbody>
        </table>
    </article>
);