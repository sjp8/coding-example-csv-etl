import React, { useState } from 'react';
import { Formik, Form, Field } from "formik";
import axios from "axios";
import { VehiclesTable } from './VehiclesTable';
import './App.css';

function App() {
  const [vehicles, setVehicles] = useState<any[]>([]);

  return (
    <div className="App">
      <header className="App-header">
        CSV File Upload Example
      </header>

      <article>
        <Formik
          initialValues={{ provider: "example", file: "" }}
          onSubmit={async (values, { setSubmitting }) => {
            const data = new FormData();
            data.append('file', values.file);
            const result = await axios.post("http://localhost:3001/api/etl/csv?provider=" + values.provider, data)
            setVehicles(result.data.vehicles);
            setSubmitting(false);
          }}
        >
          {({ setFieldValue }) => <Form>
            <div className="field-container">
              <label>File:</label>
              <input
                type="file"
                name="file"
                onChange={
                  (e: React.ChangeEvent<HTMLInputElement>) => {
                    const file = e.target.files ? e.target.files[0] : null;
                    setFieldValue("file", file);
                  }
                }
              />
            </div>
            <div className="field-container">
              <label>Provider/Config:</label>
              <Field as="select" name="provider">
                <option value="example">Example 1: configs.example.ts. Headers are "UUID,Make,Model,Color,Year". Comma-delimited</option>
                <option value="example2">Example 2: configs.example2.ts. No headers, values are uuid, make, model, color, year. Colon-delimited</option>
              </Field>
            </div>
            <div className="field-container">
              <input type='submit' value='Upload CSV' />
            </div>
          </Form>}
        </Formik>
      </article>
      {vehicles && <VehiclesTable vehicles={vehicles} />}
    </div>
  );
}

export default App;
