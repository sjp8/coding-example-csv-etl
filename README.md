# Coding Challenge
### John-Paul Simonis

## Running the Server Example

* `cd backend`
* `npm install`
* `npm start`
* Access the API directly via `http://localhost:3001/api/loadcsv?provider=<provider name>` with a POST body containing the CSV file.
* Tests: `npm test`

## An example client-side form

* Make sure the backend is running in a separate terminal.
* `cd example`
* `npm install`
* `npm run start`
* Open the web page at `http://localhost:3000/` and select `testfiles/test.csv` for example as a file upload from this repository's folder.

## Summary

An endpoint that receives potentially large CSV files via a file upload (POST), transforming the data into a static set of columns, and loading into an in-memory MongoDB and persisting in a JSON file.

## `/api/etl/csv`  Parameters:

* files: `file`. POST body containing CSV file.
* querystring: `provider`. The name of the provider that sent the file. The ETL operation will
  use a matching configuration file to select columns from the CSV and transform them to the required columns.

### Columns:

* UUID --> `uuid`, string
* VIN (alphanumerical vehicle id) --> `vin`, string
* Make --> `make`, string
* Model --> `model`, string
* Mileage --> `mileage`, number
* Year --> `year`, number
* Price --> `price`, number (dollars). Source may be in cents or dollars, based on configuration.
* Zip Code --> `zipcode`, string
* Create Date --> `created`, string. ISO-8601 datetime. Example: "2020-06-12T00:07:45Z"
* Update Date --> `updated`, string. ISO-8601 datetime.

## Thought Process

This is a standard ETL operation. The potential for large file sizes suggests that perhaps utilizing an existing ETL library with streaming file reading would help to avoid problems with larger file uploads. While Cloud provider offerings would be a go-to for reliable multi-GB uploads, for this interview question, a smaller ETL library should do the trick. Apache Spark is a well-known multi-purpose data processing platform, but it does not have Node.js support.

If I were developing a solution for a product that was certain to see multi-GB uploads, I would separate the REST endpoint from the ETL node for example by publishing each row as it streams to a pub/sub/MQ engine like Kafka, redis or Google Pub/Sub. I would also consider supporting the uploading of files to a storage bucket then passing the file URL instead of a raw file in the request.

Let's continue to research some non-Cloud ETL npm packages.

* https://smartive.github.io/proc-that/ is a small library with a declarative syntax directly corresponding to ETL. It is a little out of date. It doesn't seem to support CSV out of the box.
* https://www.npmjs.com/package/etl provides control over batching, supports CSV and MongoDB, and provides data transformations that are likely to come in handy when converting date and other formats. Fits the problem exactly.

Time to add express and create a REST endpoint.

Adding TypeScript to save time reading documentation.

Adding `express-fileupload` middleware to express, which uses `busboy` internally, which does streaming file uploads, perhaps ideal for larger files.

Adding a config file. Because the transform operations can be so specific given varied sources of data, I decided on javascript for the config file format. The config object is a map of column names to transforming functions. A transforming function takes an arbitrary input, and outputs either a value of the target type, or null if the input was invalid or not present.

Adding mongoose and mongodb-memory-server, and a model for a Vehicle for insertion.

Using a Node.js Read Stream to pipe the file content to the etl library, which allows for the stream to be split into lines. Using a basic synchronous csv parser (csv-parser/lib/sync) to parse line by line. It has a streaming option which was worth exploring as an overall alternative to etl. `etl` has useful constructs but is poorly documented and does not provide typescript definitions. I would also have further researched Node.js ReadStream and done a custom pipes implementation.

Adding jest and ts-jest to test the etl component in isolation with an example file (test.csv) and example config, which supports four fields.

## Takeaways:

* ETL really is in the Cloud, e.g. Kafka, DataFlow, Spark. Libraries are not maintained.
* Streams provide a useful paradigm for asynchronously processing a file in a non-blocking manner.
* Files are uploaded to a tmp directory which may not allow for a large file upload given the container disk limits. Research methods of transferring multi-GB files.
* Using a typescript configuration file does limit the ability for example load a configuration file from a remote URL. All providers' configurations would have to be included in source control.
